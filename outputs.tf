output "id" {
  value       = aws_ecs_service.this.id
  description = "The Amazon Resource Name (ARN) that identifies the service."
}

output "name" {
  value       = aws_ecs_service.this.name
  description = "The name of the created ECS service."
}

output "cluster" {
  value       = aws_ecs_service.this.cluster
  description = "The Amazon Resource Name (ARN) of cluster which the service runs on."
}

output "iam_role" {
  value       = aws_ecs_service.this.iam_role
  description = "The ARN of IAM role used for LB."
}

output "desired_count" {
  value       = aws_ecs_service.this.desired_count
  description = "The number of instances of the task definition."
}

output "task_arn" {
  value       = aws_ecs_task_definition.this.arn
  description = "The arn of the task definition including family and revision"
} 
