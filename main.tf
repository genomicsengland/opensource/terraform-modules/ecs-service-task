resource "aws_ecs_service" "this" {
  name                               = var.name
  cluster                            = var.cluster_arn
  deployment_maximum_percent         = var.deployment_maximum_percent
  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  desired_count                      = var.desired_count
  task_definition                    = aws_ecs_task_definition.this.arn
  enable_ecs_managed_tags            = var.enable_ecs_managed_tags
  health_check_grace_period_seconds  = var.health_check_grace_period_seconds
  launch_type                        = var.launch_type
  platform_version                   = var.platform_version
  propagate_tags                     = var.propagate_tags
  scheduling_strategy                = var.scheduling_strategy
  enable_execute_command             = var.enable_execute_command
  wait_for_steady_state              = var.wait_for_steady_state

  deployment_controller {
    type = var.deployment_controller_type
  }

  dynamic "load_balancer" {
    for_each = var.load_balancer
    content {
      target_group_arn = load_balancer.value.target_group_arn
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
    }
  }

  dynamic "placement_constraints" {
    for_each = var.placement_constraints
    content {
      type = placement_constraints.value.type
      # the bellow value is optional
      expression = lookup(placement_constraints.value, "expression", null)
    }
  }

  dynamic "ordered_placement_strategy" {
    for_each = var.ordered_placement_strategy
    content {
      type = ordered_placement_strategy.value.type
      # the bellow value is optional
      field = lookup(ordered_placement_strategy.value, "field", null)
    }
  }

  dynamic "network_configuration" {
    for_each = var.network_configuration
    content {
      subnets          = network_configuration.value.network_subnets
      security_groups  = network_configuration.value.security_groups
      assign_public_ip = network_configuration.value.network_assign_public_ip
    }
  }

  dynamic "service_registries" {
    for_each = var.service_registries
    content {
      registry_arn = service_registries.value["registry_arn"]
      # the below values are optional
      port           = lookup(service_registries.value, "port", null)
      container_port = lookup(service_registries.value, "container_port", null)
      container_name = lookup(service_registries.value, "container_name", null)
    }
  }

  dynamic "service_connect_configuration" {
    for_each = length(var.service_connect_configuration) > 0 ? [var.service_connect_configuration] : []

    content {
      enabled = try(service_connect_configuration.value.enabled, true)

      dynamic "log_configuration" {
        for_each = try([service_connect_configuration.value.log_configuration], [])

        content {
          log_driver = try(log_configuration.value.log_driver, null)
          options    = try(log_configuration.value.options, null)

          dynamic "secret_option" {
            for_each = try(log_configuration.value.secret_option, [])

            content {
              name       = secret_option.value.name
              value_from = secret_option.value.value_from
            }
          }
        }
      }

      namespace = lookup(service_connect_configuration.value, "namespace", null)

      dynamic "service" {
        for_each = try([service_connect_configuration.value.service], [])

        content {

          dynamic "client_alias" {
            for_each = try([service.value.client_alias], [])

            content {
              dns_name = try(client_alias.value.dns_name, null)
              port     = client_alias.value.port
            }
          }

          discovery_name        = try(service.value.discovery_name, null)
          ingress_port_override = try(service.value.ingress_port_override, null)
          port_name             = service.value.port_name
        }
      }
    }
  }

  # This block cannot be dynamic. Also it cannot use a variable
  lifecycle {
    ignore_changes = [
      desired_count
    ]
  }
  tags = var.tags
}

resource "aws_ecs_task_definition" "this" {
  container_definitions    = var.container_definitions
  family                   = var.family
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.task_role_arn
  network_mode             = var.network_mode
  requires_compatibilities = var.requires_compatibilities

  dynamic "volume" {
    for_each = var.volume
    content {
      name      = volume.value["name"]
      host_path = lookup(volume.value, "host_path", null)

      dynamic "efs_volume_configuration" {
        for_each = length(keys(lookup(volume.value, "efs_volume_configuration", {}))) == 0 ? [] : [lookup(volume.value, "efs_volume_configuration", {})]
        content {
          file_system_id          = efs_volume_configuration.value["file_system_id"]
          root_directory          = lookup(efs_volume_configuration.value, "root_directory", null)
          transit_encryption      = lookup(efs_volume_configuration.value, "transit_encryption", null)
          transit_encryption_port = lookup(efs_volume_configuration.value, "transit_encryption_port", null)
          authorization_config {
            access_point_id = lookup(efs_volume_configuration.value, "access_point_id", null)
            iam             = lookup(efs_volume_configuration.value, "iam", null)
          }
        }
      }
    }
  }

  dynamic "proxy_configuration" {
    for_each = var.proxy_configuration
    content {
      container_name = proxy_configuration.value["container_name"]
      properties     = proxy_configuration.value["properties"]
      type           = "APPMESH"
    }
  }

  dynamic "inference_accelerator" {
    for_each = var.inference_accelerator
    content {
      device_name = inference_accelerator.value["device_name"]
      device_type = inference_accelerator.value["device_type"]
    }
  }

  tags = var.tags
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_capacity_provider
resource "aws_ecs_capacity_provider" "this" {
  count = var.aws_ecs_capacity_provider_enable ? 1 : 0
  name  = var.name

  auto_scaling_group_provider {
    auto_scaling_group_arn         = var.aws_autoscaling_group_arn
    managed_termination_protection = var.managed_termination_protection

    managed_scaling {
      instance_warmup_period    = var.instance_warmup_period
      maximum_scaling_step_size = var.maximum_scaling_step_size
      minimum_scaling_step_size = var.minimum_scaling_step_size
      status                    = var.status
      target_capacity           = var.target_capacity
    }
  }

  tags = var.tags
}
