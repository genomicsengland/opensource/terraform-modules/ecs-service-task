#--------------------------------------------------------------
# Common
#--------------------------------------------------------------

variable "name" {
  description = "A string for naming resources."
  type        = string
}

# tflint-ignore: terraform_unused_declarations
variable "permissions_boundary" {
  description = "ARN of the policy that is used to set the permissions boundary for the role."
  type        = string
  default     = null
}

variable "tags" {
  description = "A map of tags to associate to the resource."
  type        = map(string)
  default     = {}
}

#--------------------------------------------------------------"
# ECS Service
#--------------------------------------------------------------

variable "cluster_arn" {
  description = "ARN of an ECS cluster"
  type        = string
}

variable "deployment_maximum_percent" {
  description = "The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment. Not valid when using the DAEMON scheduling strategy."
  type        = number
  default     = 200
}

variable "deployment_minimum_healthy_percent" {
  description = "The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment."
  type        = number
  default     = 100
}

variable "desired_count" {
  description = "The number of instances of the task definition."
  type        = number
  default     = 1
}

variable "enable_ecs_managed_tags" {
  description = "Specifies whether to enable Amazon ECS managed tags for the tasks within the service."
  type        = bool
  default     = false
}

variable "health_check_grace_period_seconds" {
  description = "Health check Grace priod before container is killed"
  type        = string
  default     = null
}

variable "launch_type" {
  description = "The launch type on which to run your service. The valid values are EC2 and FARGATE."
  type        = string
  default     = "FARGATE"
}

variable "platform_version" {
  description = "The platform version on which to run your service. Only applicable for launch_type set to FARGATE. LATEST uses 1.3.0."
  type        = string
  default     = "1.4.0"
}

variable "propagate_tags" {
  description = "Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid values are SERVICE and TASK_DEFINITION."
  type        = string
  default     = null
}

variable "scheduling_strategy" {
  description = "The scheduling strategy to use for the service. The valid values are REPLICA and DAEMON. Defaults to REPLICA. Fargate tasks do not support the DAEMON scheduling strategy."
  type        = string
  default     = "REPLICA"
}

variable "deployment_controller_type" {
  description = "Type of deployment controller. Valid values: CODE_DEPLOY, ECS. Default: ECS."
  type        = string
  default     = "ECS"
}

variable "load_balancer" {
  description = "A load balancer block."
  type        = list(map(string))
  default     = []
}

variable "placement_constraints" {
  description = "Rules that are taken into consideration during task placement. Maximum number of placement_constraints is 10. conflicts with ordered_placement_strategy"
  type        = list(map(string))
  default     = []
}

variable "ordered_placement_strategy" {
  description = "Rules that are taken into consideration during task placement. Maximum number of placement_constraints is 10. conflicts with placement_constraints"
  type        = list(map(string))
  default     = []
}

variable "service_registries" {
  description = "The service discovery registries for the service. The maximum number of service_registries blocks is 1."
  type        = list(map(string))
  default     = []
}

variable "service_connect_configuration" {
  description = "The ECS Service Connect configuration for this service to discover and connect to services, and be discovered by, and connected from, other services within a namespace"
  type        = any
  default     = {}
}

variable "enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service."
  type        = bool
  default     = false
}

variable "wait_for_steady_state" {
  description = "Terraform will wait for the service to reach a steady state before continuing."
  type        = bool
  default     = false
}

#--------------------------------------------------------------
# Task definition
#--------------------------------------------------------------
variable "container_definitions" {
  description = "A list of valid container definitions provided as a single valid JSON document."
  type        = string
}

variable "family" {
  description = "A unique name for your task definition."
  type        = string
}

variable "cpu" {
  description = "The number of cpu units used by the task."
  type        = number
  default     = 512
}

variable "memory" {
  description = "The amount (in MiB) of memory used by the task."
  type        = number
  default     = 1024
}

variable "execution_role_arn" {
  description = "The ARN of the task execution role that the Amazon ECS container agent and the Docker daemon can assume."
  type        = string
  default     = null
}

variable "task_role_arn" {
  description = "The ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services."
  type        = string
  default     = null
}

variable "network_mode" {
  description = "Docker networking mode to use for the containers in the task. Valid values are none, bridge, awsvpc, and host."
  type        = string
  default     = "awsvpc"
}

variable "network_configuration" {
  description = "Network configuration for the service. This parameter is required for task definitions that use the awsvpc network mode to receive their own Elastic Network Interface, and it is not supported for other network modes."
  type        = any
  default     = []
}

variable "requires_compatibilities" {
  description = "Set of launch types required by the task. The valid values are EC2 and FARGATE."
  type        = list(string)
  default     = ["FARGATE"]
}

variable "volume" {
  description = "A set of volume blocks that containers in your task may use."
  type        = any
  default     = []
}

# tflint-ignore: terraform_unused_declarations
variable "efs_volume_configuration" {
  description = "This parameter is specified when using Amazon EFS volumes."
  type        = list(any)
  default     = []
}

variable "proxy_configuration" {
  description = "The configuration details for the App Mesh proxy."
  type        = list(any)
  default     = []
}

variable "inference_accelerator" {
  description = "Inference Accelerators settings."
  type        = list(map(string))
  default     = []
}

#--------------------------------------------------------------
# Capacity provider
#--------------------------------------------------------------
variable "aws_ecs_capacity_provider_enable" {
  description = "Provides an ECS cluster capacity provider."
  type        = bool
  default     = false
}

variable "aws_autoscaling_group_arn" {
  description = "ARN of the associated auto scaling group."
  type        = string
  default     = null
}

variable "managed_termination_protection" {
  description = "Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens. If omitted, the default value of 300 seconds is used."
  type        = string
  default     = null
}

variable "instance_warmup_period" {
  description = "Period of time, in seconds, after a newly launched Amazon EC2 instance can contribute to CloudWatch metrics for Auto Scaling group. If this parameter is omitted, the default value of 300 seconds is used."
  type        = string
  default     = null
}

variable "maximum_scaling_step_size" {
  description = "Maximum step adjustment size. A number between 1 and 10,000."
  type        = string
  default     = null
}

variable "minimum_scaling_step_size" {
  description = "Minimum step adjustment size. A number between 1 and 10,000."
  type        = string
  default     = null
}

variable "status" {
  description = "Whether auto scaling is managed by ECS."
  type        = string
  default     = null
}

variable "target_capacity" {
  description = "Target utilization for the capacity provider. A number between 1 and 100."
  type        = string
  default     = null
}

#--------------------------------------------------------------
# Autoscaling policies
#--------------------------------------------------------------
variable "autoscaling_enable" {
  description = "Enable or disable autoscaling."
  type        = bool
  default     = false
}

variable "autoscaling_max_capacity" {
  description = "The max capacity of the scalable target."
  type        = number
  default     = 4
}

variable "autoscaling_min_capacity" {
  description = "The min capacity of the scalable target."
  type        = number
  default     = 1
}

variable "autoscaling_resource_id" {
  description = "The resource type and unique identifier string for the resource associated with the scaling policy."
  type        = string
  default     = null
}

variable "autoscaling_cpu_enable" {
  description = "Enable or disable autoscaling based on CPU utilization."
  type        = bool
  default     = false
}

variable "autoscaling_target_value_cpu_utilization" {
  description = "The target value for the metric."
  type        = number
  default     = 75
}

variable "autoscaling_memory_enable" {
  description = "Enable or disable autoscaling based on memory utilization."
  type        = bool
  default     = false
}

variable "autoscaling_target_value_memory_utilization" {
  description = "The target value for the metric."
  type        = number
  default     = 60
}

variable "autoscaling_custom_metrics" {
  description = "Add custom Autoscaling based on external metrics"
  type = list(object({
    name = string

    target_value       = number
    scale_in_cooldown  = number
    scale_out_cooldown = number

    metric_name = string
    namespace   = string
    statistic   = string
    unit        = string

    dimensions = list(object({
      name  = string
      value = string
    }))
  }))

  default = []
}
#--------------------------------------------------------------
# CloudWatch
#--------------------------------------------------------------
variable "cloudwatch_enable" {
  description = "Create cloudwatch log group"
  type        = bool
  default     = true
}

variable "cloudwatch_name" {
  description = "The log group to which the awslogs log driver sends its log streams."
  type        = string
}

variable "cloudwatch_retention_in_days" {
  description = "Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  type        = number
  default     = 30
}

variable "cloudwatch_kms_key_id" {
  description = "The ARN of the KMS Key to use when encrypting log data."
  type        = string
  default     = null
}
