resource "aws_appautoscaling_target" "this" {
  count = var.autoscaling_enable ? 1 : 0

  max_capacity       = var.autoscaling_max_capacity
  min_capacity       = var.autoscaling_min_capacity
  resource_id        = var.autoscaling_resource_id
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "this_cpu_utilization" {
  count = var.autoscaling_cpu_enable ? 1 : 0

  name               = "${var.name}-CpuTargetTrackingPolicy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.this[count.index].resource_id
  scalable_dimension = aws_appautoscaling_target.this[count.index].scalable_dimension
  service_namespace  = aws_appautoscaling_target.this[count.index].service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = var.autoscaling_target_value_cpu_utilization
  }
  depends_on = [aws_appautoscaling_target.this]
}

resource "aws_appautoscaling_policy" "this_memory_utilization" {
  count = var.autoscaling_memory_enable ? 1 : 0

  name               = "${var.name}-MemoryTargetTrackingPolicy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.this[count.index].resource_id
  scalable_dimension = aws_appautoscaling_target.this[count.index].scalable_dimension
  service_namespace  = aws_appautoscaling_target.this[count.index].service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value = var.autoscaling_target_value_memory_utilization
  }
  depends_on = [aws_appautoscaling_target.this]
}

resource "aws_appautoscaling_policy" "this_custom" {
  for_each = { for cm in var.autoscaling_custom_metrics : cm.name => cm }

  name               = "${each.key}-CustomTargetTrackingPolicy"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.this[0].resource_id
  scalable_dimension = aws_appautoscaling_target.this[0].scalable_dimension
  service_namespace  = aws_appautoscaling_target.this[0].service_namespace

  target_tracking_scaling_policy_configuration {
    target_value = each.value.target_value

    scale_in_cooldown  = each.value.scale_in_cooldown
    scale_out_cooldown = each.value.scale_out_cooldown

    customized_metric_specification {
      metric_name = each.value.metric_name
      namespace   = each.value.namespace
      statistic   = each.value.statistic
      unit        = each.value.unit

      dynamic "dimensions" {
        for_each = each.value.dimensions
        content {
          name  = dimensions.value.name
          value = dimensions.value.value
        }
      }
    }
  }
}
