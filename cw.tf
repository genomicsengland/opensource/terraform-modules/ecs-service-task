resource "aws_cloudwatch_log_group" "this" {
  count             = var.cloudwatch_enable ? 1 : 0
  name              = var.cloudwatch_name
  retention_in_days = var.cloudwatch_retention_in_days
  kms_key_id        = var.cloudwatch_kms_key_id

  tags = var.tags
}
