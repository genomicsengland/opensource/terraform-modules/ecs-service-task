# AWS ECS Service Task Definition Terraform Module

## Description

AWS Terraform module deploying ECS service with defined task definition to fargate or EC2.

Addionally following can be enabled/disable as per requirements:
- cloudwatch log group
- capacity provider configuration
- task autoscaling

## Usage

```
module "ecs_service" {
  source = "git::https://gitlab.com/genomicsengland/opensource/terraform-modules/ecs-service-task.git?ref=v1.0.4"

  name                  = "service-name"
  cluster_arn           = module.ecs.ecs_cluster_arn
  execution_role_arn    = module.ecs_task_execution_role.iam_role_arn
  task_role_arn         = module.ecs_task_role.iam_role_arn
  subnets               = data.aws_subnet_ids.private.ids
  security_groups       = [module.service_name.security_group_id]
  container_definitions = data.template_file.web_container_definition.rendered
  family                = "family-name"
  cloudwatch_name       = "/aws/ecs/service-name"
}
```

## Prerequisites

Following software should be available to work with the repository:

- [Terraform](https://releases.hashicorp.com/terraform/)
- [AWS provider](https://www.terraform.io/docs/providers/aws/index.html)

Following software should be available to add changes/modify the repository:

- [terraform-docs](https://github.com/segmentio/terraform-docs/releases)
- [pre-commit](https://pre-commit.com/#install)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/liamg/tfsec)

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12.6 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.50 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.61.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_appautoscaling_policy.this_cpu_utilization](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_policy.this_memory_utilization](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_policy) | resource |
| [aws_appautoscaling_target.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/appautoscaling_target) | resource |
| [aws_cloudwatch_log_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_ecs_capacity_provider.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_capacity_provider) | resource |
| [aws_ecs_service.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |
| [aws_ecs_task_definition.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Assign a public IP address to the ENI (Fargate launch type only). | `bool` | `false` | no |
| <a name="input_autoscaling_cpu_enable"></a> [autoscaling\_cpu\_enable](#input\_autoscaling\_cpu\_enable) | Enable or disable autoscaling based on CPU utilization. | `bool` | `false` | no |
| <a name="input_autoscaling_enable"></a> [autoscaling\_enable](#input\_autoscaling\_enable) | Enable or disable autoscaling. | `bool` | `false` | no |
| <a name="input_autoscaling_max_capacity"></a> [autoscaling\_max\_capacity](#input\_autoscaling\_max\_capacity) | The max capacity of the scalable target. | `number` | `4` | no |
| <a name="input_autoscaling_memory_enable"></a> [autoscaling\_memory\_enable](#input\_autoscaling\_memory\_enable) | Enable or disable autoscaling based on memory utilization. | `bool` | `false` | no |
| <a name="input_autoscaling_min_capacity"></a> [autoscaling\_min\_capacity](#input\_autoscaling\_min\_capacity) | The min capacity of the scalable target. | `number` | `1` | no |
| <a name="input_autoscaling_resource_id"></a> [autoscaling\_resource\_id](#input\_autoscaling\_resource\_id) | The resource type and unique identifier string for the resource associated with the scaling policy. | `string` | `null` | no |
| <a name="input_autoscaling_target_value_cpu_utilization"></a> [autoscaling\_target\_value\_cpu\_utilization](#input\_autoscaling\_target\_value\_cpu\_utilization) | The target value for the metric. | `number` | `75` | no |
| <a name="input_autoscaling_target_value_memory_utilization"></a> [autoscaling\_target\_value\_memory\_utilization](#input\_autoscaling\_target\_value\_memory\_utilization) | The target value for the metric. | `number` | `60` | no |
| <a name="input_aws_autoscaling_group_arn"></a> [aws\_autoscaling\_group\_arn](#input\_aws\_autoscaling\_group\_arn) | ARN of the associated auto scaling group. | `string` | `null` | no |
| <a name="input_aws_ecs_capacity_provider_enable"></a> [aws\_ecs\_capacity\_provider\_enable](#input\_aws\_ecs\_capacity\_provider\_enable) | Provides an ECS cluster capacity provider. | `bool` | `false` | no |
| <a name="input_cloudwatch_enable"></a> [cloudwatch\_enable](#input\_cloudwatch\_enable) | Create cloudwatch log group | `bool` | `true` | no |
| <a name="input_cloudwatch_kms_key_id"></a> [cloudwatch\_kms\_key\_id](#input\_cloudwatch\_kms\_key\_id) | The ARN of the KMS Key to use when encrypting log data. | `string` | `null` | no |
| <a name="input_cloudwatch_name"></a> [cloudwatch\_name](#input\_cloudwatch\_name) | The log group to which the awslogs log driver sends its log streams. | `string` | n/a | yes |
| <a name="input_cloudwatch_retention_in_days"></a> [cloudwatch\_retention\_in\_days](#input\_cloudwatch\_retention\_in\_days) | Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653. | `number` | `30` | no |
| <a name="input_cluster_arn"></a> [cluster\_arn](#input\_cluster\_arn) | ARN of an ECS cluster | `string` | n/a | yes |
| <a name="input_container_definitions"></a> [container\_definitions](#input\_container\_definitions) | A list of valid container definitions provided as a single valid JSON document. | `string` | n/a | yes |
| <a name="input_cpu"></a> [cpu](#input\_cpu) | The number of cpu units used by the task. | `number` | `512` | no |
| <a name="input_deployment_controller_type"></a> [deployment\_controller\_type](#input\_deployment\_controller\_type) | Type of deployment controller. Valid values: CODE\_DEPLOY, ECS. Default: ECS. | `string` | `"ECS"` | no |
| <a name="input_deployment_maximum_percent"></a> [deployment\_maximum\_percent](#input\_deployment\_maximum\_percent) | The upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment. Not valid when using the DAEMON scheduling strategy. | `number` | `200` | no |
| <a name="input_deployment_minimum_healthy_percent"></a> [deployment\_minimum\_healthy\_percent](#input\_deployment\_minimum\_healthy\_percent) | The lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment. | `number` | `100` | no |
| <a name="input_desired_count"></a> [desired\_count](#input\_desired\_count) | The number of instances of the task definition. | `number` | `1` | no |
| <a name="input_efs_volume_configuration"></a> [efs\_volume\_configuration](#input\_efs\_volume\_configuration) | This parameter is specified when using Amazon EFS volumes. | `list(any)` | `[]` | no |
| <a name="input_enable_ecs_managed_tags"></a> [enable\_ecs\_managed\_tags](#input\_enable\_ecs\_managed\_tags) | Specifies whether to enable Amazon ECS managed tags for the tasks within the service. | `bool` | `false` | no |
| <a name="input_enable_execute_command"></a> [enable_execute_command](#input\_enable\_execute\_command) | Specifies whether to enable Amazon ECS Exec for the tasks within the service. | `bool` | `false` | no |
| <a name="input_execution_role_arn"></a> [execution\_role\_arn](#input\_execution\_role\_arn) | The ARN of the task execution role that the Amazon ECS container agent and the Docker daemon can assume. | `string` | `null` | no |
| <a name="input_family"></a> [family](#input\_family) | A unique name for your task definition. | `string` | n/a | yes |
| <a name="input_health_check_grace_period_seconds"></a> [health\_check\_grace\_period\_seconds](#input\_health\_check\_grace\_period\_seconds) | Health check Grace priod before container is killed | `string` | `null` | no |
| <a name="input_inference_accelerator"></a> [inference\_accelerator](#input\_inference\_accelerator) | Inference Accelerators settings. | `list(map(string))` | `[]` | no |
| <a name="input_instance_warmup_period"></a> [instance\_warmup\_period](#input\_instance\_warmup\_period) | Period of time, in seconds, after a newly launched Amazon EC2 instance can contribute to CloudWatch metrics for Auto Scaling group. If this parameter is omitted, the default value of 300 seconds is used. | `string` | `null` | no |
| <a name="input_launch_type"></a> [launch\_type](#input\_launch\_type) | The launch type on which to run your service. The valid values are EC2 and FARGATE. | `string` | `"FARGATE"` | no |
| <a name="input_load_balancer"></a> [load\_balancer](#input\_load\_balancer) | A load balancer block. | `list(map(string))` | `[]` | no |
| <a name="input_managed_termination_protection"></a> [managed\_termination\_protection](#input\_managed\_termination\_protection) | Enables or disables container-aware termination of instances in the auto scaling group when scale-in happens. If omitted, the default value of 300 seconds is used. | `string` | `null` | no |
| <a name="input_maximum_scaling_step_size"></a> [maximum\_scaling\_step\_size](#input\_maximum\_scaling\_step\_size) | Maximum step adjustment size. A number between 1 and 10,000. | `string` | `null` | no |
| <a name="input_memory"></a> [memory](#input\_memory) | The amount (in MiB) of memory used by the task. | `number` | `1024` | no |
| <a name="input_minimum_scaling_step_size"></a> [minimum\_scaling\_step\_size](#input\_minimum\_scaling\_step\_size) | Minimum step adjustment size. A number between 1 and 10,000. | `string` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | A string for naming resources. | `string` | n/a | yes |
| <a name="input_network_mode"></a> [network\_mode](#input\_network\_mode) | Docker networking mode to use for the containers in the task. Valid values are none, bridge, awsvpc, and host. | `string` | `"awsvpc"` | no |
| <a name="input_permissions_boundary"></a> [permissions\_boundary](#input\_permissions\_boundary) | ARN of the policy that is used to set the permissions boundary for the role. | `string` | `null` | no |
| <a name="input_placement_constraints"></a> [placement\_constraints](#input\_placement\_constraints) | Rules that are taken into consideration during task placement. Maximum number of placement\_constraints is 10. | `list(map(string))` | `[]` | no |
| <a name="input_platform_version"></a> [platform\_version](#input\_platform\_version) | The platform version on which to run your service. Only applicable for launch\_type set to FARGATE. LATEST uses 1.3.0. | `string` | `"1.4.0"` | no |
| <a name="input_propagate_tags"></a> [propagate\_tags](#input\_propagate\_tags) | Specifies whether to propagate the tags from the task definition or the service to the tasks. The valid values are SERVICE and TASK\_DEFINITION. | `string` | `null` | no |
| <a name="input_proxy_configuration"></a> [proxy\_configuration](#input\_proxy\_configuration) | The configuration details for the App Mesh proxy. | `list(any)` | `[]` | no |
| <a name="input_requires_compatibilities"></a> [requires\_compatibilities](#input\_requires\_compatibilities) | Set of launch types required by the task. The valid values are EC2 and FARGATE. | `list(string)` | <pre>[<br>  "FARGATE"<br>]</pre> | no |
| <a name="input_scheduling_strategy"></a> [scheduling\_strategy](#input\_scheduling\_strategy) | The scheduling strategy to use for the service. The valid values are REPLICA and DAEMON. Defaults to REPLICA. Fargate tasks do not support the DAEMON scheduling strategy. | `string` | `"REPLICA"` | no |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | The security groups associated with the task or service. If you use a load balancer, you must allow access. | `list(string)` | n/a | yes |
| <a name="input_service_registries"></a> [service\_registries](#input\_service\_registries) | The service discovery registries for the service. The maximum number of service\_registries blocks is 1. | `list(map(string))` | `[]` | no |
| <a name="input_status"></a> [status](#input\_status) | Whether auto scaling is managed by ECS. | `string` | `null` | no |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | The subnets associated with the task or service. | `list(string)` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to associate to the resource. | `map(string)` | `{}` | no |
| <a name="input_target_capacity"></a> [target\_capacity](#input\_target\_capacity) | Target utilization for the capacity provider. A number between 1 and 100. | `string` | `null` | no |
| <a name="input_task_role_arn"></a> [task\_role\_arn](#input\_task\_role\_arn) | The ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services. | `string` | `null` | no |
| <a name="input_volume"></a> [volume](#input\_volume) | A set of volume blocks that containers in your task may use. | `list(any)` | `[]` | no |
| <a name="input_wait_for_steady_state"></a> [wait_for_steady_state](#input\_wait\_for\_steady\_state) | Terraform will wait for the service to reach a steady state before continuing. | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster"></a> [cluster](#output\_cluster) | The Amazon Resource Name (ARN) of cluster which the service runs on. |
| <a name="output_desired_count"></a> [desired\_count](#output\_desired\_count) | The number of instances of the task definition. |
| <a name="output_iam_role"></a> [iam\_role](#output\_iam\_role) | The ARN of IAM role used for LB. |
| <a name="output_id"></a> [id](#output\_id) | The Amazon Resource Name (ARN) that identifies the service. |
| <a name="output_name"></a> [name](#output\_name) | The name of the created ECS service. |
<!-- END_TF_DOCS -->
